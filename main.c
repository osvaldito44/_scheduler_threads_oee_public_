#include "scheduler.h"

void *reproducir_musica(void *params);
void *descargando_archivo(void *params);
void *proseso_general(void *params);

int main(int argc, char const *argv[]){
	task *t1 = crear_Task(1, UN_SEGUNDO, "Tarea_1", "Reproducir Música", ACTIVO, reproducir_musica);
	task *t2 = crear_Task(1, DOS_SEGUNDOS, "Tarea_2", "Abrir Youtube", NO_ACTIVO, proseso_general);
	task *t3 = crear_Task(3, TRES_SEGUNDOS, "Tarea_3", "Escribir Texto en Word", ACTIVO, proseso_general);
	task *t4 = crear_Task(4, TRES_SEGUNDOS, "Tarea_4", "Descargando Archivo", ACTIVO, descargando_archivo);
	task *t5 = crear_Task(3, TRES_SEGUNDOS, "Tarea_5", "Subiendo Archivo", ACTIVO, proseso_general);
	task *t6 = crear_Task(4, TRES_SEGUNDOS, "Tarea_6", "Compilando programa", NO_ACTIVO, proseso_general);
	task *t7 = crear_Task(0, TRES_SEGUNDOS, "Tarea_7", "Ejecutando programa", ACTIVO, proseso_general);
	task *t8 = crear_Task(2, CUATRO_SEGUNDOS, "Tarea_8", "Usando la terminal", ACTIVO, proseso_general);
	task *t9 = crear_Task(5, CUATRO_SEGUNDOS, "Tarea_9", "Usando la terminal", NO_ACTIVO, proseso_general);

	array_task *array = crear_ArrayTasks();

	agregar_Task(array, *t1);
	agregar_Task(array, *t2);
	agregar_Task(array, *t3);
	agregar_Task(array, *t4);
	agregar_Task(array, *t5);
	agregar_Task(array, *t6);
	agregar_Task(array, *t7);
	agregar_Task(array, *t8);
	agregar_Task(array, *t9);

	ejecutar_Tasks(array);

	free(array);
	free(t1);
	free(t2);
	free(t3);
	free(t4);
	free(t5);
	free(t6);
	free(t7);
	free(t8);
	free(t9);

	return 0;
}

void *reproducir_musica(void *params){
	char *proceso_realizar;
	proceso_realizar = (char *)params;
	printf("\t ======> EJECUTANDO PROCESO: [%s]\n", proceso_realizar);
	pthread_exit(NULL);
}

void *descargando_archivo(void *params){
	char *proceso_realizar;
	proceso_realizar = (char *)params;
	printf("\t ======> EJECUTANDO PROCESO: [%s]\n", proceso_realizar);
	pthread_exit(NULL);
}

void *proseso_general(void *params){
	char *proceso_realizar;
	proceso_realizar = (char *)params;
	printf("\t ======> EJECUTANDO PROCESO: [%s]\n", proceso_realizar);
	pthread_exit(NULL);
}
